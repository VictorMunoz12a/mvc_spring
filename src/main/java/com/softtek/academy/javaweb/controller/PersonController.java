package com.softtek.academy.javaweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDao;

@Controller
public class PersonController { 
	@Autowired 
	private PersonDao Dao;
	
	
	@RequestMapping(value = "/person",  method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
		
	}
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        Dao.saveStudent(person);
        return "newPerson";
    }
}
